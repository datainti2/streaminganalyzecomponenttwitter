// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-base-widget',
//   templateUrl: './base-widget.component.html',
//   styleUrls: ['./base-widget.component.css']
// })
// export class BaseWidgetComponent implements OnInit {
//   public dataLoader: Array<any> = [
// 	{
		
// 		"image": "../../assets/default.jpg",
// 		"screen_name": "15 minutes ago",
// 		"timestamp": "Agence France-Presse",
// 		"tweet_text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//   {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//   {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//    {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//    {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
// ];

//   constructor() {console.log(this.dataLoader);}
  
//   ngOnInit() { 
//   }

// }





import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
  public dataLoader: Array<any> = [
	 {
      "screen_name" : "@ernakarina",
      "timestamp" : "1 hours ago",
      "image" : "assets/img/foto1.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    },
    {
      "screen_name" : "@liviaaniston",
      "timestamp" : "2 hours ago",
      "image" : "assets/img/foto2.jpg",
      "tweet_text":"API (Application Programming Interface) adalah sekumpulan perintah, fungsi, komponen, dan protokol yang disediakan oleh sistem operasi ataupun bahasa pemrograman tertentu yang dapat digunakan oleh programmer saat membangun perangkat lunak"
    },
     {
      "screen_name" : "@medialisar",
      "timestamp" : "3 hours ago",
      "image" : "assets/img/foto3.jpg",
      "tweet_text":"Udara adalah suatu campuran gas yang terdapat pada lapisan yang mengelilingi bumi."
    },
     {
      "screen_name" : "@nadayolanda",
      "timestamp" : "4 hours ago",
      "image" : "assets/img/foto4.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    },
     {
      "screen_name" : "@indraaries",
      "timestamp" : "5 hours ago",
      "image" : "assets/img/foto5.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    }, {
      "screen_name" : "@akbarwahyudi",
      "timestamp" : "6 hours ago",
      "image" : "assets/img/foto6.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    }

    ];

    

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}